objects := $(patsubst %.cpp,%.o,$(wildcard *.cpp))

ROOTCFLAGS    = $(shell root-config --cflags)
CXXFLAGS     += $(ROOTCFLAGS) -I../analysis_core  -D__STANDALONE__
CXX           = $(shell root-config --cxx) -g


%.o : %.cxx
	$(CXX) $(CXXFLAGS) -c $< -o $@

test : basic_parser.exe
#	root  -q -b -x makeSos.C
	./basic_parser.exe
clean:
	rm -f *.o ; rm -f *.so ; rm -rf *.d ; rm -rf *.exe
build : $(objects)  basic_parser.exe
basic_parser.exe : $(objects) 
	g++ basic_parser.cpp -o basic_parser.exe
cleanup:
	rm -f *.o ; rm -f *.so ; rm -rf *.d ; rm -rf *.exe

install:
	cp basic_parser.exe /tmp/
